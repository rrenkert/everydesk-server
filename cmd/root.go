// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"bitbucket.org/rrenkert/everydesk-server/database"
	"bitbucket.org/rrenkert/everydesk-server/service"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string
var host string
var port int

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "everydesk-server",
	Short: "Server component of the everywhere browser desktop",
	Long:  ``,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("Everydesk is listening on %s:%d\n", viper.GetString("network.host"), viper.GetInt("network.port"))
		n := service.Start()
		database.Connect()
		n.Run(fmt.Sprintf("%s:%d", viper.GetString("network.host"), viper.GetInt("network.port")))
	},
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports Persistent Flags, which, if defined here,
	// will be global for your application.

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is /etc/everydesk/everydesk.toml)")
	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	RootCmd.Flags().StringVarP(&host, "host", "H", "", "Host address to listen to.")
	RootCmd.Flags().IntVarP(&port, "port", "p", -1, "Port to listen to.")

	//viper.BindPFlag("port", RootCmd.Flags().Lookup("port"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(cfgFile)
	}
	initDefaults()

	viper.SetConfigName("everydesk") // name of config file (without extension)
	viper.AddConfigPath("/etc/everydesk")
	viper.AddConfigPath("$HOME/.everydesk")
	viper.AddConfigPath(".")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("No config file found.")
	}
	initStringFlag("host", "network")
	initIntFlag("port", "network")
}

func initDefaults() {
	viper.SetDefault("network", map[string]interface{}{
		"host": "localhost",
		"port": 8080,
	})
	viper.SetDefault("couchdb", map[string]interface{}{
		"host":     "localhost",
		"port":     5984,
		"database": "everydesk",
		"user":     "admin",
		"password": "secret",
	})
}

func initStringFlag(key, group string) {
	flag := RootCmd.Flags().Lookup(key)
	if flag != nil && flag.Changed {
		viper.Set(group+"."+key, flag.Value)
	}
}

func initIntFlag(key, group string) {
	flag := RootCmd.Flags().Lookup(key)
	if flag != nil && flag.Changed {
		value, err := strconv.ParseInt(flag.Value.String(), 10, 64)
		if err != nil {
			log.Fatal("Invalid input for %s: %s", key, flag.Value.String())
		}
		viper.Set(group+"."+key, value)
	}
}
