// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"errors"
	"math/rand"
	"time"
)

func GenerateIV() string {
	s := make([]byte, 16)
	for i := 0; i < 16; i++ {
		r := rand.New(rand.NewSource(time.Now().UnixNano()))
		rNum := r.Intn(len(chars))
		s[i] = chars[rNum]
	}
	return string(s[:16])
}

func EncryptAES(key, iv, password []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	b := base64.StdEncoding.EncodeToString(password)
	ciphertext := make([]byte, aes.BlockSize+len(b))
	encrypter := cipher.NewCFBEncrypter(block, iv)
	encrypter.XORKeyStream(ciphertext[aes.BlockSize:], []byte(b))
	return ciphertext, nil
}

func DecryptAES(key, iv, password []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	if len(password) < aes.BlockSize {
		return nil, errors.New("ciphertext too short")
	}
	password = password[aes.BlockSize:]
	decrypter := cipher.NewCFBDecrypter(block, iv)
	decrypter.XORKeyStream(password, password)
	data, err := base64.StdEncoding.DecodeString(string(password))
	if err != nil {
		return nil, err
	}
	return data, nil
}
