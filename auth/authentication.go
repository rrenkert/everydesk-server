// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	"net/http"
	"time"

	"context"

	"bitbucket.org/rrenkert/everydesk-server/util"
)

type authenticatedContextType int

const AuthenticatedSession authenticatedContextType = 0

const (
	sessionHeader    = "X-EVERYDESK-SESSION"
	sessionParameter = "EVERYDESK-SESSION"
)

type Authentication struct {
	sessions *Sessions
}

func NewAuthentication(maxAge time.Duration) *Authentication {
	return &Authentication{
		sessions: NewSessions(maxAge)}
}

func (a *Authentication) CreateSession() *Session {
	return a.sessions.CreateSession()
}

func (a *Authentication) DeleteSession(key string) {
	a.sessions.DeleteSession(key)
}

func (a *Authentication) KillSessions(kill func(*Session) bool) {
	a.sessions.KillSessions(kill)
}

func (a *Authentication) ServeHTTP(rw http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	sessionKey := extractSessionKey(req)
	if sessionKey == "" {
		util.HTTPMsg(rw, http.StatusUnauthorized)
		return
	}

	session := a.sessions.GetSession(sessionKey)
	if session == nil {
		util.HTTPMsg(rw, http.StatusUnauthorized)
		return
	}

	ctx := req.Context()
	ctx = context.WithValue(ctx, AuthenticatedSession, session)
	req = req.WithContext(ctx)

	// Call the next middleware handler
	next(rw, req)
}
