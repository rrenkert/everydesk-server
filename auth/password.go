// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	crand "crypto/rand"
	"io"
	"math/rand"

	"golang.org/x/crypto/bcrypt"
)

const chars = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
	"0123456789_"

func pseudoRandomPassword(n int) string {
	s := ""
	for i := 0; i < n; i++ {
		s += string(chars[rand.Intn(len(chars))])
	}
	return s
}

func GeneratePassword(n int) string {
	b := make([]byte, n)
	// Fall back to pseudo random password.
	if _, err := io.ReadFull(crand.Reader, b); err != nil {
		return pseudoRandomPassword(n)
	}

	s := ""
	for i := 0; i < n; i++ {
		s += string(chars[int(b[i])%len(chars)])
	}
	return s
}

func HashPassword(pw string) (hash string) {
	hashArray, _ := bcrypt.GenerateFromPassword([]byte(pw), bcrypt.DefaultCost)
	hash = string(hashArray[:len(hashArray)])
	return
}

func CorrectPassword(pw, hash string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(pw)) == nil
}
