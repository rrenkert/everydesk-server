// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	"errors"
	"fmt"
	"log"

	"github.com/jinzhu/gorm"

	"bitbucket.org/rrenkert/everydesk-server/backend"
	"bitbucket.org/rrenkert/everydesk-server/database"
)

const userIdKey = "user-id"

func GetUsers() (*database.User, error) {
	db, err := database.Connect()
	if err != nil {
		return nil, err
	}
	defer db.Close()
	foundUsers := database.User{}
	db.Find(&foundUsers)
	return &foundUsers, nil
}

func GetUserByUserName(username string) (*database.User, error) {
	db, err := database.Connect()
	if err != nil {
		return nil, err
	}
	defer db.Close()
	found := database.User{}
	err = db.Where("Username = ?", username).First(&found).Error
	if err != nil {
		return nil, err
	}
	return &found, nil
}

func GetUserById(id int64) (*database.User, error) {
	db, err := database.Connect()
	if err != nil {
		return nil, err
	}
	defer db.Close()
	found := database.User{}
	db.Where("ID = ?", id).Find(&found)
	if err != nil {
		return nil, err
	}
	return &found, nil
}

func GetUserByUserMail(mail string) (*database.User, error) {
	db, err := database.Connect()
	if err != nil {
		return nil, err
	}
	defer db.Close()
	found := database.User{}
	db.Where("Email = ?", mail).Find(&found)
	if err != nil {
		return nil, err
	}
	return &found, nil
}

func CreateUser(user *database.User) error {
	found, err := GetUserByUserName(user.Username)
	if found != nil {
		return fmt.Errorf("User already exists")
	}
	db, err := database.Connect()
	if err != nil {
		return err
	}
	defer db.Close()
	db.Create(&user)
	return nil
}

func UpdateUser(user *database.User) error {
	found, err := GetUserById(user.ID)
	if err != nil {
		return err
	}
	if found == nil {
		return fmt.Errorf("User does not exist")
	}
	db, err := database.Connect()
	if err != nil {
		return err
	}
	defer db.Close()
	found.Active = user.Active
	found.Admin = user.Admin
	found.AesIv = user.AesIv
	found.Email = user.Email
	found.Forename = user.Forename
	found.Secret = user.Secret
	found.Surname = user.Surname
	found.Username = user.Username
	db.Save(&found)
	return nil
}

func DeleteUser(user *database.User) error {
	found, err := GetUserById(user.ID)
	if err != nil {
		return err
	}
	if found == nil {
		return fmt.Errorf("User does not exist")
	}
	db, err := database.Connect()
	if err != nil {
		return err
	}
	defer db.Close()
	db.Delete(&user)
	return nil
}

func GetUser(username string) (user *database.User, err error) {
	if user, err = GetUserByUserName(username); err != nil {
		log.Printf("DB: user not found: %s\n", username)
		return nil, err
	}

	if user == nil { //|| !user.Active || !CorrectPassword(password, user.Secret) {
		return nil, errors.New("no user...")
	}
	return user, nil
}

func AuthenticateLDAP(username, password string, a *Authentication) (bool, error) {
	client := backend.Ldap{}
	client.Connect()
	ok, err := client.Authenticate(username, password)
	if err != nil {
		return false, err
	}
	if ok {
		_, err := GetUser(username)
		if err != nil && gorm.IsRecordNotFoundError(err) {
			u, err := client.GetUser(username)
			if err != nil {
				log.Printf("LDAP error: %s\n", err.Error())
				return false, err
			}
			newUser := database.User{
				Username: u["uid"],
				Forename: u["givenName"],
				Surname:  u["sn"],
				Email:    u["mail"],
				Active:   true,
				Admin:    false,
				AesIv:    GenerateIV(),
			}
			CreateUser(&newUser)
		}
		if err != nil && !gorm.IsRecordNotFoundError(err) {
			return false, err
		}
		return true, nil
	}
	return false, errors.New("wrong password")
}

func GetUserSession(user *database.User, a *Authentication) (session map[string]interface{}, err error) {
	if !user.Active {
		return nil, errors.New("user not active\n")
	}
	return createSession(user, a)
}

func createSession(user *database.User, a *Authentication) (session map[string]interface{}, err error) {
	var s *Session
	if s = a.CreateSession(); s == nil {
		return nil, errors.New("no session")
	}

	s.Put(userIdKey, user.ID)

	v := map[string]interface{}{
		"key":  s.Key,
		"user": user.Username}
	return v, nil
}
