// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	"crypto/rand"
	"encoding/base64"
	"io"
	"net/http"
	"sync"
	"time"
)

const keyLength = 24

type Session struct {
	Key  string
	last time.Time
	mu   sync.RWMutex
	kv   map[string]interface{}
}

type Sessions struct {
	maxAge   time.Duration
	sessions map[string]*Session
	mu       sync.Mutex
}

func NewSession(key string) *Session {
	return &Session{
		Key:  key,
		last: time.Now(),
		kv:   make(map[string]interface{})}
}

func (s *Session) GetOk(key string) (value interface{}, found bool) {
	s.mu.RLock()
	defer s.mu.RUnlock()
	value, found = s.kv[key]
	return
}

func (s *Session) Get(key string) interface{} {
	s.mu.RLock()
	defer s.mu.RUnlock()
	return s.kv[key]
}

func (s *Session) Put(key string, value interface{}) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.kv[key] = value
}

func (s *Session) Delete(key string) {
	s.mu.Lock()
	defer s.mu.Unlock()
	delete(s.kv, key)
}

func NewSessions(maxAge time.Duration) *Sessions {
	s := &Sessions{
		maxAge:   maxAge,
		sessions: make(map[string]*Session)}

	// Throw away expired sessions every 15 mins.
	go func() {
		c := time.Tick(15 * time.Minute)
		for now := range c {
			s.removeExpired(now)
		}
	}()

	return s
}

func (s *Sessions) removeExpired(now time.Time) {
	s.mu.Lock()
	defer s.mu.Unlock()
	for key, session := range s.sessions {
		expired := session.last.Add(s.maxAge)
		if expired.Before(now) {
			delete(s.sessions, key)
		}
	}
}

func (s *Sessions) DeleteSession(key string) {
	s.mu.Lock()
	defer s.mu.Unlock()
	delete(s.sessions, key)
}

func (s *Sessions) GetSession(key string) *Session {
	s.mu.Lock()
	defer s.mu.Unlock()
	session, found := s.sessions[key]
	if !found {
		return nil
	}
	now := time.Now()
	expired := session.last.Add(s.maxAge)
	if expired.Before(now) {
		delete(s.sessions, key)
		return nil
	}
	session.last = now
	return session
}

func (s *Sessions) KillSessions(kill func(*Session) bool) {
	s.mu.Lock()
	defer s.mu.Unlock()
	for k, x := range s.sessions {
		if kill(x) {
			delete(s.sessions, k)
		}
	}
}

func (s *Sessions) CreateSession() *Session {
	s.mu.Lock()
	defer s.mu.Unlock()

	for {
		var err error
		var key string
		if key, err = generateKey(keyLength); err != nil {
			return nil
		}
		if _, found := s.sessions[key]; !found {
			session := NewSession(key)
			s.sessions[key] = session
			return session
		}
	}
}

func GetSession(req *http.Request) *Session {
	return req.Context().Value(AuthenticatedSession).(*Session)
}

func generateKey(n int) (string, error) {
	b := make([]byte, n)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(b), nil
}

func extractSessionKey(req *http.Request) string {
	sessionKey := req.Header.Get(sessionHeader)
	if sessionKey == "" {
		return req.FormValue(sessionParameter)
	}
	return sessionKey
}
