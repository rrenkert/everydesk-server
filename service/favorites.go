// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"log"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"bitbucket.org/rrenkert/everydesk-server/database"
	"bitbucket.org/rrenkert/everydesk-server/util"
	"github.com/gorilla/mux"
)

func handleGetFavorites(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	favorites := []*database.Favorite{}
	db.Model(&user).Preload("FavoriteType").Related(&favorites)
	if err = util.SendJSON(rw, favorites); err != nil {
		log.Printf("Send failed: %s\n", err.Error())
	}
}

func handlePutFavorites(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	var newFavorite database.Favorite
	if err := util.DecodeJSON(r, &newFavorite); err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	newFavorite.UserID = userID
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		// TODO
		util.SendJSON(rw, "error")
		return
	}
	var favorite database.Favorite
	db.Model(&user).Preload("FavoriteType").Where("ID = ?", newFavorite.ID).First(&favorite)
	if favorite.ID != 0 {
		favorite.Name = newFavorite.Name
		favorite.Endpoint = newFavorite.Endpoint
		favorite.FavoriteTypeID = newFavorite.FavoriteTypeID
		favorite.Icon = newFavorite.Icon
		favorite.Itemgroup = newFavorite.Itemgroup
		favorite.URI = newFavorite.URI
		db.Set("gorm:association_autoupdate", false).Save(&favorite)
	} else {
		db.NewRecord(&newFavorite)
		db.Create(&newFavorite)
	}
	var favorites []*database.Favorite
	db.Model(&user).Preload("FavoriteType").Related(&favorites)
	util.SendJSON(rw, &favorites)
}

func handlePostFavorites(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	handlePutFavorites(rw, r, a)
}

func handleDeleteFavorites(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		// TODO
		util.SendJSON(rw, "error")
		return
	}
	v := strings.Split(string(r.URL.Path), "/")
	if len(v) > 1 {
		id, err := strconv.ParseInt(v[2], 10, 64)
		if err != nil {
			util.SendJSON(rw, "Error parsing the id")
			return
		}
		var favorite database.Favorite
		db.Model(&user).Preload("FavoriteType").Where("ID = ?", id).First(&favorite)
		if favorite.ID != 0 {
			db.Delete(favorite)
			var favorites []*database.Favorite
			db.Model(&user).Related(&favorites)
			util.SendJSON(rw, &favorites)
			return
		}
		util.SendJSON(rw, "not existing")
	}
}

func ServeFavorites(router, secureRouter *mux.Router, a *auth.Authentication) {
	route := secureRouter.Path("/favorites").Subrouter()
	paramRoute := secureRouter.Path("/favorites/{id:[a-zA-Z0-9\\.]+}").Subrouter()
	route.Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleGetFavorites(rw, r, a)
		})
	route.Methods("PUT").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePutFavorites(rw, r, a)
		})
	route.Methods("POST").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePostFavorites(rw, r, a)
		})
	paramRoute.Methods("DELETE").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleDeleteFavorites(rw, r, a)
		})
}
