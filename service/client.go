// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"fmt"
	"net/http"

	"github.com/spf13/viper"

	"github.com/gorilla/mux"
)

func ServeClient(router *mux.Router) {
	fmt.Println("Serving client from " + viper.GetString("client.basePath"))
	router.PathPrefix("/client-dev/").Handler(http.StripPrefix("/client-dev/", http.FileServer(http.Dir(viper.GetString("client.basePath")))))
	router.PathPrefix("/client/").Handler(http.StripPrefix("/client/", http.FileServer(http.Dir(viper.GetString("client.basePath")+"build/"))))
}
