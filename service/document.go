// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"

	"bitbucket.org/rrenkert/everydesk-server/backend"
	"bitbucket.org/rrenkert/everydesk-server/database"

	"github.com/gorilla/mux"
	"github.com/spf13/viper"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"bitbucket.org/rrenkert/everydesk-server/util"
)

func handleGetDocument(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	data := make(map[string]interface{})
	if err := util.DecodeJSON(r, &data); err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	ret := make(map[string]interface{})

	var y int = int(data["status"].(float64))
	switch y {
	case 2:
		handleSave(rw, r, a, data)
	case 3:
		handleSave(rw, r, a, data)
	case 4:
		handleUnchanged(rw, r, a, data)
	case 6:
		handleUpload(rw, data)
	default:
		fmt.Println("no match")
	}

	ret["error"] = 0
	if err := util.SendJSON(rw, ret); err != nil {
		log.Printf("Send failed: %s\n", err.Error())
	}
}

func handleSave(rw http.ResponseWriter, r *http.Request, a *auth.Authentication, data map[string]interface{}) {
	// request file from document server
	resp, err := http.Get(data["url"].(string))
	if err != nil {
		ret := make(map[string]interface{})
		ret["error"] = 1
		if err := util.SendJSON(rw, ret); err != nil {
			log.Printf("Send failed: %s\n", err.Error())
		}
		return
	}
	defer resp.Body.Close()
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	// Read the body
	fileData, err := ioutil.ReadAll(resp.Body)

	// Find the filesystem where the original file is stored
	// and fix the file extension
	fileSystem := findFilesystem(rw, r, a)

	// Prepare put request data
	reader := ioutil.NopCloser(bytes.NewReader(fileData))
	requestPath := strings.TrimPrefix(r.URL.Path, "/document/edit/")
	requestPath = fixFileExtension(requestPath)
	requestPath = strings.TrimPrefix(requestPath, fileSystem.Path)

	pass64, err := base64.StdEncoding.DecodeString(fileSystem.Password)
	if err != nil {
		fmt.Println(err)
	}
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	if err != nil {
		fmt.Println(err)
	}
	if fileSystem.FileserviceType.Name == "webdav" {
		webdav, err := backend.NewWebdav(
			fileSystem.Url,
			fileSystem.Login,
			string(pass),
		)
		if err != nil {
			util.SendJSON(rw, err)
			return
		}
		webdav.SaveReader(requestPath, reader)
	}
	if fileSystem.FileserviceType.Name == "dropbox" {
		dbox := backend.NewDBox(
			string(pass),
		)
		dbox.SaveReader(requestPath, reader)
	}
	// Build response fo the document server
	ret := make(map[string]interface{})
	ret["error"] = 0
	if err := util.SendJSON(rw, ret); err != nil {
		log.Printf("Send failed: %s\n", err.Error())
	}
	return
}

func handleNewDocument(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	fileSystem := findFilesystem(rw, r, a)
	requestPath := strings.TrimPrefix(r.URL.Path, "/document/new/"+fileSystem.Path)
	var newFile map[string]interface{}
	if err := util.DecodeJSON(r, &newFile); err != nil {
		fmt.Println("new file err")
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	fileType := newFile["type"]
	emptyName := "empty.docx"
	switch fileType {
	case "document":
		emptyName = "empty.docx"
	case "spreadsheet":
		emptyName = "empty.xlsx"
	case "presentation":
		emptyName = "empty.pptx"
	}
	bodyBytes, err := util.Asset(emptyName)
	if err != nil {
		ret := make(map[string]interface{})
		ret["error"] = 1
		fmt.Println("error sending document to webdav!")
		util.SendJSON(rw, ret)
	}
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	pass64, err := base64.StdEncoding.DecodeString(fileSystem.Password)
	if err != nil {
		fmt.Println(err)
	}
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	if err != nil {
		fmt.Println(err)
	}
	if fileSystem.FileserviceType.Name == "webdav" {
		webdav, err := backend.NewWebdav(
			fileSystem.Url,
			fileSystem.Login,
			string(pass),
		)
		if err != nil {
			ret := make(map[string]interface{})
			ret["error"] = 1
			fmt.Println("error sending document to webdav!")
			util.SendJSON(rw, ret)
			return
		}
		webdav.SaveReader(requestPath, bytes.NewReader(bodyBytes))
	}
	if fileSystem.FileserviceType.Name == "dropbox" {
		dbox := backend.NewDBox(
			string(pass),
		)
		dbox.SaveReader(requestPath, bytes.NewReader(bodyBytes))
	}
	ret := make(map[string]interface{})
	ret["error"] = 0
	if err := util.SendJSON(rw, ret); err != nil {
		log.Printf("Send failed: %s\n", err.Error())
	}
	return
}

func handleUpload(rw http.ResponseWriter, data map[string]interface{}) {
	fmt.Println("handle upload")
}

func handleUnchanged(rw http.ResponseWriter, r *http.Request, a *auth.Authentication, data map[string]interface{}) {
	// Find the filesystem where the original file is stored
	// and fix the file extension
	webdav := findFilesystem(rw, r, a)

	// Build the request path for filesystem
	requestPath := strings.TrimPrefix(r.URL.Path, "/document/"+webdav.Path)
	requestUrl := webdav.Url + requestPath

	resp, err := http.Get(requestUrl)
	if err != nil {
		ret := make(map[string]interface{})
		ret["error"] = 1
		if err := util.SendJSON(rw, ret); err != nil {
			log.Printf("Send failed: %s\n", err.Error())
		}
		return
	}
	defer resp.Body.Close()

	// Read the body
	fileData, err := ioutil.ReadAll(resp.Body)

	// Prepare put request data
	reader := ioutil.NopCloser(bytes.NewReader(fileData))

	// Build https client request
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := http.Client{Transport: tr}
	postRequest, _ := http.NewRequest("PUT", requestUrl, reader)
	basicAuth := base64.StdEncoding.EncodeToString([]byte(webdav.Login + ":" + webdav.Password))
	postRequest.Header.Set("Authorization", "Basic "+basicAuth)
	postRequest.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	// Execute the request
	_, err = client.Do(postRequest)
	if err != nil {
		fmt.Println(err)
		ret := make(map[string]interface{})
		ret["error"] = 1
		fmt.Println("error sending document to webdav!")
		util.SendJSON(rw, ret)
	}

	// Build response fo the document server
	ret := make(map[string]interface{})
	ret["error"] = 0
	if err := util.SendJSON(rw, ret); err != nil {
		log.Printf("Send failed: %s\n", err.Error())
	}
	return
}

func findFilesystem(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) *database.Fileservice {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return nil
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return nil
	}
	path := strings.Split(r.URL.Path, "/")
	fileservice := database.Fileservice{}
	db.Model(&user).Preload("FileserviceType").Where("Path = ?", path[3]).Related(&fileservice)
	return &fileservice
}

func fixFileExtension(path string) string {
	// TODO
	if strings.HasSuffix(path, util.ODT) {
		return strings.TrimSuffix(path, util.ODT) + "docx"
	}
	if strings.HasSuffix(path, util.ODS) {
		return strings.TrimSuffix(path, util.ODS) + "xlsx"
	}
	if strings.HasSuffix(path, util.ODP) {
		return strings.TrimSuffix(path, util.ODP) + "pptx"
	}
	return path
}

func handleProxy(rw http.ResponseWriter, r *http.Request) {
	prefix := "/document/server"
	r.URL.Path = strings.TrimPrefix(r.URL.Path, prefix)
	r.Header.Add("X-Forwarded-Host", r.Host+prefix)
	r.Header.Add("X-Forwarded-Proto", r.URL.Scheme)
	r.Header.Add("X-Forwarded-For", viper.GetString("documentserver.url"))
	url, err := url.Parse("http://" + viper.GetString("documentserver.url") + ":" + viper.GetString("documentserver.port"))
	if !IsWebSocketRequest(r) {
		if err != nil {
			util.HTTPMsg(rw, http.StatusInternalServerError)
			return
		}
		proxy := httputil.NewSingleHostReverseProxy(url)
		proxy.ServeHTTP(rw, r)
	} else {
		websocketProxy(rw, r, viper.GetString("documentserver.url")+":"+viper.GetString("documentserver.port"))
	}
}

func ServeDocument(router, secureRouter *mux.Router, a *auth.Authentication) {
	routeEdit := secureRouter.PathPrefix("/document/edit/").Subrouter()
	routeNew := secureRouter.PathPrefix("/document/new/").Subrouter()
	router.PathPrefix("/document/server").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleProxy(rw, r)
		})
	routeEdit.Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleGetDocument(rw, r, a)
		})
	routeEdit.Methods("POST").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleGetDocument(rw, r, a)
		})
	routeNew.Methods("POST").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleNewDocument(rw, r, a)
		})
}
