// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"log"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"bitbucket.org/rrenkert/everydesk-server/database"
	"bitbucket.org/rrenkert/everydesk-server/util"
	"github.com/gorilla/mux"
)

func handleGetWorkspaces(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	workspaces := []*database.Workspace{}
	db.Model(&user).Find(&workspaces)
	if err = util.SendJSON(rw, workspaces); err != nil {
		log.Printf("Send failed: %s\n", err.Error())
	}
}

func handlePutWorkspace(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	var newWorkspace database.Workspace
	if err := util.DecodeJSON(r, &newWorkspace); err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	newWorkspace.UserID = userID
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		// TODO
		util.SendJSON(rw, "error")
		return
	}
	var workspace database.Workspace
	db.Model(&user).Where("ID = ?", newWorkspace.ID).First(&workspace)
	if workspace.ID != 0 {
		workspace.Name = newWorkspace.Name
		workspace.State = newWorkspace.State
		db.Save(&workspace)
	} else {
		db.NewRecord(&newWorkspace)
		db.Create(&newWorkspace)
	}
	var workspaces []*database.Workspace
	db.Model(&user).Find(&workspaces)
	util.SendJSON(rw, &workspaces)
}

func handlePostWorkspace(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	handlePutWorkspace(rw, r, a)
}

func handleDeleteWorkspace(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		// TODO
		util.SendJSON(rw, "error")
		return
	}
	v := strings.Split(string(r.URL.Path), "/")
	if len(v) > 1 {
		id, err := strconv.ParseInt(v[2], 10, 64)
		if err != nil {
			util.SendJSON(rw, "Error parsing the id")
			return
		}
		var workspace database.Workspace
		db.Model(&user).Where("ID = ?", id).First(&workspace)
		if workspace.ID != 0 {
			db.Delete(workspace)
			var workspaces *database.Workspace
			db.Model(&user).Find(&workspaces)
			util.SendJSON(rw, &workspaces)
			return
		}
		util.SendJSON(rw, "not existing")
	}
}

func ServeWorkspace(router, secureRouter *mux.Router, a *auth.Authentication) {
	route := secureRouter.Path("/workspace").Subrouter()
	paramRoute := secureRouter.Path("/workspace/{id:[a-zA-Z0-9\\.]+}").Subrouter()
	route.Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleGetWorkspaces(rw, r, a)
		})
	route.Methods("PUT").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePutWorkspace(rw, r, a)
		})
	route.Methods("POST").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePostWorkspace(rw, r, a)
		})
	paramRoute.Methods("DELETE").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleDeleteWorkspace(rw, r, a)
		})
}
