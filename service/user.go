// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"bitbucket.org/rrenkert/everydesk-server/backend"
	"bitbucket.org/rrenkert/everydesk-server/database"
	"bitbucket.org/rrenkert/everydesk-server/util"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

func handleGetUser(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	if !user.Admin {
		if err = util.SendJSON(rw, "{}"); err != nil {
			log.Printf("Send failed: %s\n", err.Error())
		}
		return
	}

	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	var users []*database.User
	err = db.Find(&users).Error
	if err != nil {
		fmt.Println(err)
	}
	// TODO check if apps contains any data
	fmt.Println(&users)
	if err = util.SendJSON(rw, users); err != nil {
		log.Printf("Send failed: %s\n", err.Error())
	}
}

func handlePutUser(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	loggedIn, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	if !loggedIn.Admin {
		if err = util.SendJSON(rw, "{}"); err != nil {
			log.Printf("Send failed: %s\n", err.Error())
		}
		return
	}

	var newUser database.User
	if err := util.DecodeJSON(r, &newUser); err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		// TODO
		util.SendJSON(rw, "error")
		return
	}
	var user database.User
	err = db.Where("ID = ?", newUser.ID).First(&user).Error
	if gorm.IsRecordNotFoundError(err) {
		if newUser.Email == "" {
			util.SendJSON(rw, "{error: 'no email'}")
			return
		}
		password := auth.GeneratePassword(10)
		newUser.AesIv = auth.GenerateIV()
		newUser.Secret = auth.HashPassword(password)
		mail := backend.Mail{}
		err = mail.Send(newUser.Email, "Your account for everydesk:\n Username: "+newUser.Username+"\n Password: "+password+"\n")
		if err != nil {
			log.Fatal(err)
			util.SendJSON(rw, "{error: 'sending email failed'}")
			return
		}
		db.NewRecord(&newUser)
		db.Create(&newUser)
	} else {
		user.Username = newUser.Username
		user.Forename = newUser.Forename
		user.Surname = newUser.Surname
		user.Email = newUser.Email
		user.Admin = newUser.Admin
		user.Active = newUser.Active
		db.Set("gorm:association_autoupdate", false).Save(&user)
	}
	var users []database.User
	db.Find(&users)
	util.SendJSON(rw, &users)
}

func handlePostUser(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	handlePutUser(rw, r, a)
}

func handleDeleteUser(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	// TODO
}

func handleGetUserApp(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	if !user.Admin {
		if err = util.SendJSON(rw, "{}"); err != nil {
			log.Printf("Send failed: %s\n", err.Error())
		}
		return
	}

	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		// TODO
		util.SendJSON(rw, "error")
		return
	}
	v := strings.Split(string(r.URL.Path), "/")
	if len(v) > 1 {
		id, err := strconv.ParseInt(v[3], 10, 64)
		if err != nil {
			util.SendJSON(rw, "Error parsing the id")
			return
		}
		var userApp database.User
		err = db.Where("ID = ?", id).Preload("Applications").First(&userApp).Error
		if err != nil {
			util.SendJSON(rw, "{error: 'user not found'}")
			return
		}

		util.SendJSON(rw, &userApp.Applications)
		return
	}
}

func handlePostUserApp(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	if !user.Admin {
		if err = util.SendJSON(rw, "{}"); err != nil {
			log.Printf("Send failed: %s\n", err.Error())
		}
		return
	}

	var newApps []database.Application
	if err := util.DecodeJSON(r, &newApps); err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		// TODO
		util.SendJSON(rw, "error")
		return
	}
	v := strings.Split(string(r.URL.Path), "/")
	if len(v) > 1 {
		id, err := strconv.ParseInt(v[3], 10, 64)
		if err != nil {
			util.SendJSON(rw, "Error parsing the id")
			return
		}
		var userApp database.User
		err = db.Where("ID = ?", id).First(&userApp).Error
		if err != nil {
			util.SendJSON(rw, "{error: 'user not found'}")
			return
		}
		err = db.Model(&userApp).Association("Applications").Replace(newApps).Error
		if err != nil {
			log.Fatal(err)
			util.SendJSON(rw, "{error: 'update applications'}")
			return
		}
		util.SendJSON(rw, &userApp.Applications)
		return
	}
}

func ServeUser(router, secureRouter *mux.Router, a *auth.Authentication) {
	route := secureRouter.Path("/user").Subrouter()
	paramRouteApp := secureRouter.Path("/user/application/{id:[0-9\\.]+}").Subrouter()
	paramRoute := secureRouter.Path("/user/{id:[0-9\\.]+}").Subrouter()
	route.Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleGetUser(rw, r, a)
		})
	route.Methods("PUT").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePutUser(rw, r, a)
		})
	route.Methods("POST").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePostUser(rw, r, a)
		})
	paramRouteApp.Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleGetUserApp(rw, r, a)
		})
	paramRouteApp.Methods("POST").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePostUserApp(rw, r, a)
		})
	paramRoute.Methods("DELETE").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleDeleteUser(rw, r, a)
		})
}
