// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/spf13/viper"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"bitbucket.org/rrenkert/everydesk-server/database"
	"bitbucket.org/rrenkert/everydesk-server/util"
	"github.com/gorilla/mux"
)

type login struct {
	User     string
	Password string
}

func handleLogin(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	username, password := r.FormValue("user"), r.FormValue("password")
	if username == "" {
		decoder := json.NewDecoder(r.Body)
		fmt.Println(r.Body)
		var l *login
		err := decoder.Decode(&l)
		if err != nil {
			log.Fatal("parser error in login ", err)
			return
		}
		username = l.User
		password = l.Password
	}
	authMethod := viper.GetString("auth.method")
	var user *database.User
	if authMethod == "ldap" {
		ok, err := auth.AuthenticateLDAP(username, password, a)
		user, err := auth.GetUser(username)
		if !ok {
			if err != nil || !auth.CorrectPassword(password, user.Secret) {
				util.HTTPMsg(rw, http.StatusUnauthorized)
				return
			}
			session, err := auth.GetUserSession(user, a)
			if err != nil {
				util.HTTPMsg(rw, http.StatusUnauthorized)
				return
			}
			if err = util.SendJSON(rw, session); err != nil {
				log.Printf("sending JSON failed: %s\n", err.Error())
			}
			return
		}
		session, err := auth.GetUserSession(user, a)
		if err != nil {
			util.HTTPMsg(rw, http.StatusUnauthorized)
			return
		}
		if err = util.SendJSON(rw, session); err != nil {
			log.Printf("sending JSON failed: %s\n", err.Error())
		}
		return
	}
	user, err := auth.GetUser(username)
	if err != nil {
		util.HTTPMsg(rw, http.StatusUnauthorized)
		return
	}
	if !auth.CorrectPassword(password, user.Secret) {
		util.HTTPMsg(rw, http.StatusUnauthorized)
		return
	}

	session, err := auth.GetUserSession(user, a)
	if err != nil {
		util.HTTPMsg(rw, http.StatusUnauthorized)
		return
	}
	if err = util.SendJSON(rw, session); err != nil {
		log.Printf("sending JSON failed: %s\n", err.Error())
	}
}

func handleLogout(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	session := auth.GetSession(r)
	a.DeleteSession(session.Key)
	util.HTTPMsg(rw, http.StatusOK)
}

func ServeLogin(router, secureRouter *mux.Router, a *auth.Authentication) {
	router.Path("/login").Methods("POST").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleLogin(rw, r, a)
		})
	secureRouter.Path("/logout").Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleLogout(rw, r, a)
		})
}
