// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"fmt"
	"net/http"

	"bitbucket.org/rrenkert/everydesk-server/auth"

	"github.com/gorilla/mux"
)

func handleGetSession(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	/*	userID := auth.GetSession(r).Get(userIdKey).(int64)
		if user, err := auth.GetUserById(userID); err != nil {
			log.Printf("DB error: %s\n", err.Error())
			util.HTTPMsg(rw, http.StatusInternalServerError)
			return
		}*/
	// TODO
}

func handlePostSession(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	// TODO
}

func handleDeleteSession(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	//	decoder := json.NewDecoder(r.Body)
	fmt.Printf("%v\n", r.Body)
}

func ServeSession(router, secureRouter *mux.Router, a *auth.Authentication) {
	secureRouter.Path("/session").Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleGetSession(rw, r, a)
		})
	secureRouter.Path("/session").Methods("POST").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePostSession(rw, r, a)
		})
	secureRouter.Path("/session").Methods("DELETE").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleDeleteSession(rw, r, a)
		})
}
