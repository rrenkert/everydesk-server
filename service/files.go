// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"bytes"
	"context"
	"encoding/base64"
	"io"
	"log"
	"net/http"
	"strings"

	"golang.org/x/oauth2"

	"bitbucket.org/rrenkert/everydesk-server/backend"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"bitbucket.org/rrenkert/everydesk-server/database"
	"bitbucket.org/rrenkert/everydesk-server/util"

	"github.com/dropbox/dropbox-sdk-go-unofficial/dropbox"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

func handleGetFileservices(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	var fileservices []*database.Fileservice
	db.Model(&user).Preload("FileserviceType").Related(&fileservices)
	if err = util.SendJSON(rw, fileservices); err != nil {
		log.Printf("util.Send failed: %s\n", err.Error())
	}
}

func handlePutFileservice(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	var newFileservice database.Fileservice
	if err := util.DecodeJSON(r, &newFileservice); err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	newFileservice.UserID = userID
	newFileservice.Url = strings.TrimSuffix(newFileservice.Url, "/")
	newFileservice.SourcePath = strings.TrimSuffix(newFileservice.SourcePath, "/")
	if newFileservice.FileserviceType.Name == "webdav" {
		encrypted, err := auth.EncryptAES([]byte(serverKey), []byte(user.AesIv), []byte(newFileservice.Password))
		if err != nil {
			http.Error(rw, err.Error(), http.StatusBadRequest)
			return
		}
		newFileservice.Password = base64.StdEncoding.EncodeToString(encrypted)
		newFileservice.FileserviceType.ID = 1
	} else if newFileservice.FileserviceType.Name == "dropbox" {
		newFileservice.FileserviceType.ID = 2
		oAuthConfig := oauth2.Config{
			ClientID:     viper.GetString("dropbox.appId"),
			ClientSecret: viper.GetString("dropbox.appSecret"),
			Endpoint:     dropbox.OAuthEndpoint(""),
		}
		var token *oauth2.Token
		token, err = oAuthConfig.Exchange(context.Background(), newFileservice.Password)
		if err != nil {
			http.Error(rw, err.Error(), http.StatusBadRequest)
			return
		}
		encrypted, err := auth.EncryptAES([]byte(serverKey), []byte(user.AesIv), []byte(token.AccessToken))
		if err != nil {
			http.Error(rw, err.Error(), http.StatusBadRequest)
			return
		}
		newFileservice.Password = base64.StdEncoding.EncodeToString(encrypted)
	} else {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	fileservice := []*database.Fileservice{}
	db.Model(&user).Where("Name = ?", newFileservice.Name).Related(&fileservice)
	if len(fileservice) >= 1 {
		util.SendJSON(rw, "Allready existing")
		return
	}
	db.Create(&newFileservice)
	fileservices := database.Fileservice{}
	db.Model(&user).Related(&fileservices)
	util.SendJSON(rw, &fileservices)
}

func handlePostFileservice(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	handlePutFileservice(rw, r, a)
}

func handleDeleteFileservice(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	fileservice := []*database.Fileservice{}
	paths := strings.Split(r.URL.Path, "/")
	db.Model(&user).Where("Path = ?", paths[2]).Related(&fileservice)
	if len(fileservice) < 1 {
		util.SendJSON(rw, "Not found")
		return
	}
	db.Delete(fileservice[0])
	util.SendJSON(rw, fileservice)
}

func handleGetOAuthUrl(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	url := backend.GetDropboxOAuthUrl()
	util.SendJSON(rw, url)
}

func ServeFile(router, secureRouter *mux.Router, a *auth.Authentication) {
	route := secureRouter.Path("/fileservices").Subrouter()
	paramRoute := secureRouter.Path("/fileservices/{id:[a-zA-Z0-9\\.]+}").Subrouter()
	oauthRoute := secureRouter.Path("/dbox/oauth").Subrouter()
	extRoute := secureRouter.Path("/file/extension").Subrouter()
	route.Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleGetFileservices(rw, r, a)
		})
	route.Methods("PUT").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePutFileservice(rw, r, a)
		})
	route.Methods("POST").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePostFileservice(rw, r, a)
		})
	paramRoute.Methods("DELETE").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleDeleteFileservice(rw, r, a)
		})
	oauthRoute.Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleGetOAuthUrl(rw, r, a)
		})
	extRoute.Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			asset, err := util.Asset("everydesk-0.2-an+fx.xpi")
			if err != nil {
				util.HTTPMsg(rw, http.StatusInternalServerError)
				return
			}
			rw.Header().Add("Content-Disposition", "attachment; filename=\"everydesk-0.2-an+fx.xpi\"")
			var reader = bytes.NewBuffer(asset)
			io.Copy(rw, reader)
		})
}
