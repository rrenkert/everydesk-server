// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"log"
	"net/http"
	"os/exec"
	"sync"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"bitbucket.org/rrenkert/everydesk-server/tty"
	"bitbucket.org/rrenkert/everydesk-server/util"
	"github.com/gorilla/mux"
	"github.com/kr/pty"
)

func handleTTY(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	websocketTTY(rw, r, user.Username)
}

func websocketTTY(rw http.ResponseWriter, r *http.Request, userId string) {

	log.Printf("New client connected: %s", r.RemoteAddr)

	if r.Method != "GET" {
		http.Error(rw, "Method not allowed", 405)
		return
	}

	term, err := tty.New("docker", []string{"exec", "-it", userId, "/bin/bash"})
	if err != nil {
		log.Print("Failed to create TTY: " + err.Error())
		return
	}

	conn, err := term.Upgrader.Upgrade(rw, r, nil)
	if err != nil {
		log.Print("Failed to upgrade connection: " + err.Error())
		return
	}

	cmd := exec.Command(term.Command, term.Parameter...)
	ptyIo, err := pty.Start(cmd)
	if err != nil {
		log.Print("Failed to execute command")
		return
	}

	context := &tty.ClientContext{
		Tty:        term,
		Request:    r,
		Connection: conn,
		Command:    cmd,
		Pty:        ptyIo,
		WriteMutex: &sync.Mutex{},
	}

	context.GoHandleClient()
}

func ServeTTY(router, secureRouter *mux.Router, a *auth.Authentication) {
	secureRouter.PathPrefix("/ws").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleTTY(rw, r, a)
		})
}
