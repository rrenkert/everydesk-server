// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"log"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"bitbucket.org/rrenkert/everydesk-server/database"
	"bitbucket.org/rrenkert/everydesk-server/util"
	"github.com/gorilla/mux"
)

func handleGetSettings(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	var settings database.Settings
	db.Model(&user).First(&settings)
	if err = util.SendJSON(rw, settings); err != nil {
		log.Printf("Send failed: %s\n", err.Error())
	}
}

func handlePutSettings(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	var newSettings database.Settings
	if err := util.DecodeJSON(r, &newSettings); err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	newSettings.UserID = userID
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		// TODO
		util.SendJSON(rw, "error")
		return
	}
	var settings database.Settings
	db.Model(&user).First(&settings)
	if settings.ID != 0 {
		settings.Background = newSettings.Background
		db.Save(&settings)
	} else {
		db.NewRecord(&newSettings)
		db.Create(&newSettings)
	}
	db.Model(&user).First(&settings)
	util.SendJSON(rw, &settings)
}

func handlePostSettings(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	handlePutSettings(rw, r, a)
}

func handleDeleteSettings(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		// TODO
		util.SendJSON(rw, "error")
		return
	}
	v := strings.Split(string(r.URL.Path), "/")
	if len(v) > 1 {
		id, err := strconv.ParseInt(v[2], 10, 64)
		if err != nil {
			util.SendJSON(rw, "Error parsing the id")
			return
		}
		var settings database.Settings
		db.Model(&user).Where("ID = ?", id).First(&settings)
		if settings.ID != 0 {
			db.Delete(settings)
			var settings *database.Settings
			db.Model(&user).First(&settings)
			util.SendJSON(rw, &settings)
			return
		}
		util.SendJSON(rw, "not existing")
	}
}

func ServeSettings(router, secureRouter *mux.Router, a *auth.Authentication) {
	route := secureRouter.Path("/settings").Subrouter()
	paramRoute := secureRouter.Path("/settings/{id:[a-zA-Z0-9\\.]+}").Subrouter()
	route.Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleGetSettings(rw, r, a)
		})
	route.Methods("PUT").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePutSettings(rw, r, a)
		})
	route.Methods("POST").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePostSettings(rw, r, a)
		})
	paramRoute.Methods("DELETE").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleDeleteSettings(rw, r, a)
		})
}
