// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"encoding/base64"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"bitbucket.org/rrenkert/everydesk-server/backend"
	"bitbucket.org/rrenkert/everydesk-server/database"
	"bitbucket.org/rrenkert/everydesk-server/util"

	"github.com/gorilla/mux"
)

type InternalProxy struct {
	target               *url.URL
	proxy                *httputil.ReverseProxy
	authenticationMethod string
	credentials          string
}

func handleFileProxy(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	fileservice := database.Fileservice{}
	paths := strings.Split(r.URL.Path, "/")
	db.Model(&user).Preload("FileserviceType").Where("Path = ?", paths[2]).Related(&fileservice)
	if fileservice.FileserviceType.Name == "webdav" {
		proxyWebdav(fileservice, *user, rw, r)
	}
	if fileservice.FileserviceType.Name == "dropbox" {
		proxyDropbox(fileservice, *user, rw, r)
	}
}

func proxyDropbox(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	switch r.Method {
	case "GET":
		dbDownload(entry, user, rw, r)
	case "PUT":
		dbSave(entry, user, rw, r)
	case "PROPFIND":
		dbList(entry, user, rw, r)
	case "COPY":
		dbCopy(entry, user, rw, r)
	case "MOVE":
		dbMove(entry, user, rw, r)
	case "MKCOL":
		dbMkdir(entry, user, rw, r)
	case "DELETE":
		dbDelete(entry, user, rw, r)
	}
}

func dbDownload(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	pass64, err := base64.StdEncoding.DecodeString(entry.Password)
	if err != nil {
		fmt.Println(err)
	}
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	dbox := backend.NewDBox(string(pass))
	prefix := "/fs/" + entry.Path
	path := strings.TrimPrefix(r.URL.Path, prefix)
	dbox.Get(path, rw)
}

func dbSave(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	pass64, err := base64.StdEncoding.DecodeString(entry.Password)
	if err != nil {
		fmt.Println(err)
	}
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	dbox := backend.NewDBox(string(pass))
	prefix := "/fs/" + entry.Path
	path := strings.TrimPrefix(r.URL.Path, prefix)
	dbox.Save(path, r)
}

func dbList(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	pass64, err := base64.StdEncoding.DecodeString(entry.Password)
	if err != nil {
		fmt.Println(err)
	}
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	dbox := backend.NewDBox(string(pass))
	prefix := "/fs/" + entry.Path
	path := strings.TrimPrefix(r.URL.Path+"/", prefix)
	files, err := dbox.Ls(path, prefix)
	if err != nil {
		util.SendJSON(rw, err)
		return
	}
	util.SendJSON(rw, files)
}

func dbCopy(entry database.Fileservice, user database.User, rw http.ResponseWriter, r *http.Request) {
}

func dbMove(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	pass64, err := base64.StdEncoding.DecodeString(entry.Password)
	if err != nil {
		fmt.Println(err)
	}
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	dbox := backend.NewDBox(string(pass))
	prefix := "/fs/" + entry.Path
	path := strings.TrimPrefix(r.URL.Path, prefix)
	dest := r.Header.Get("Destination")
	if strings.HasPrefix(dest, prefix) {
		dest = strings.TrimPrefix(dest, prefix)
		dbox.Mv(path, dest)
		return
	}
	// Find target filesystem and copy the file
}

func dbMkdir(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	pass64, err := base64.StdEncoding.DecodeString(entry.Password)
	if err != nil {
		fmt.Println(err)
	}
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	dbox := backend.NewDBox(string(pass))
	prefix := "/fs/" + entry.Path
	path := strings.TrimPrefix(r.URL.Path, prefix)
	dbox.Mkdir(path)
}

func dbDelete(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	pass64, err := base64.StdEncoding.DecodeString(entry.Password)
	if err != nil {
		fmt.Println(err)
	}
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	dbox := backend.NewDBox(string(pass))
	prefix := "/fs/" + entry.Path
	path := strings.TrimPrefix(r.URL.Path, prefix)
	dbox.Rm(path)
}

func proxyWebdav(entry database.Fileservice, user database.User, rw http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		webdavDownload(entry, user, rw, r)
	case "PUT":
		webdavSave(entry, user, rw, r)
	case "PROPFIND":
		webdavList(entry, user, rw, r)
	case "COPY":
		//		webdavCopy(config, rw, r)
	case "MOVE":
		webdavMove(entry, user, rw, r)
	case "MKCOL":
		webdavMkdir(entry, user, rw, r)
	case "DELETE":
		webdavDelete(entry, user, rw, r)
	}
}

func webdavList(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	pass64, err := base64.StdEncoding.DecodeString(entry.Password)
	if err != nil {
		fmt.Println(err)
	}
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	if err != nil {
		fmt.Println(err)
	}
	webdav, err := backend.NewWebdav(
		entry.Url,
		entry.Login,
		string(pass),
	)
	prefix := "/fs/" + entry.Path
	path := strings.TrimPrefix(r.URL.Path, prefix)
	files, err := webdav.Ls(path, entry.SourcePath, prefix)
	if err != nil {
		util.SendJSON(rw, err)
		return
	}
	util.SendJSON(rw, files)
}

func webdavDownload(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	pass64, err := base64.StdEncoding.DecodeString(entry.Password)
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	webdav, err := backend.NewWebdav(
		entry.Url,
		entry.Login,
		string(pass),
	)
	if err != nil {
		fmt.Println(err)
	}
	prefix := "/fs/" + entry.Path
	path := strings.TrimPrefix(r.URL.Path, prefix)
	webdav.Get(path, prefix, rw, r)
}

func webdavSave(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	pass64, err := base64.StdEncoding.DecodeString(entry.Password)
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	webdav, err := backend.NewWebdav(
		entry.Url,
		entry.Login,
		string(pass),
	)
	if err != nil {
		fmt.Println(err)
	}
	prefix := "/fs/" + entry.Path
	path := strings.TrimPrefix(r.URL.Path, prefix)
	webdav.Save(path, rw, r)
}

func webdavDelete(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	pass64, err := base64.StdEncoding.DecodeString(entry.Password)
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	webdav, err := backend.NewWebdav(
		entry.Url,
		entry.Login,
		string(pass),
	)
	if err != nil {
		fmt.Println(err)
	}
	prefix := "/fs/" + entry.Path
	path := strings.TrimPrefix(r.URL.Path, prefix)
	webdav.Rm(path)
}

func webdavMove(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	pass64, err := base64.StdEncoding.DecodeString(entry.Password)
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	webdav, err := backend.NewWebdav(
		entry.Url,
		entry.Login,
		string(pass),
	)
	if err != nil {
		fmt.Println(err)
	}
	prefix := "/fs/" + entry.Path
	src := strings.TrimPrefix(r.URL.Path, prefix)
	dest := r.Header.Get("Destination")
	if strings.HasPrefix(dest, prefix) {
		dest := strings.TrimPrefix(dest, prefix)
		webdav.Mv(src, dest)
	}
	// Find target filesystem and copy the file
}

func webdavMkdir(
	entry database.Fileservice,
	user database.User,
	rw http.ResponseWriter,
	r *http.Request,
) {
	pass64, err := base64.StdEncoding.DecodeString(entry.Password)
	pass, err := auth.DecryptAES([]byte(serverKey), []byte(user.AesIv), pass64)
	webdav, err := backend.NewWebdav(
		entry.Url,
		entry.Login,
		string(pass),
	)
	if err != nil {
		fmt.Println(err)
	}
	prefix := "/fs/" + entry.Path
	path := strings.TrimPrefix(r.URL.Path, prefix)
	webdav.Mkdir(path)
}

func websocketProxy(w http.ResponseWriter, r *http.Request, target string) {
	d, err := net.Dial("tcp", target)
	if err != nil {
		http.Error(w, "Error contacting backend server.", 500)
		log.Printf("Error dialing websocket backend %s: %v", target, err)
		return
	}
	hj, ok := w.(http.Hijacker)
	if !ok {
		http.Error(w, "Not a hijacker?", 500)
		return
	}
	nc, _, err := hj.Hijack()
	if err != nil {
		log.Printf("Hijack error: %v", err)
		return
	}
	defer nc.Close()
	defer d.Close()

	err = r.Write(d)
	if err != nil {
		log.Printf("Error copying request to target: %v", err)
		return
	}

	errc := make(chan error, 2)
	cp := func(dst io.Writer, src io.Reader) {
		_, err := io.Copy(dst, src)
		errc <- err
	}
	go cp(d, nc)
	go cp(nc, d)
	<-errc
}

// IsWebSocketRequest returns a boolean indicating whether the request has the
// headers of a WebSocket handshake request.
func IsWebSocketRequest(r *http.Request) bool {
	contains := func(key, val string) bool {
		vv := strings.Split(r.Header.Get(key), ",")
		for _, v := range vv {
			if val == strings.ToLower(strings.TrimSpace(v)) {
				return true
			}
		}
		return false
	}
	if !contains("Connection", "upgrade") {
		return false
	}
	if !contains("Upgrade", "websocket") {
		return false
	}
	return true
}

func singleJoiningSlash(a, b string) string {
	aslash := strings.HasSuffix(a, "/")
	bslash := strings.HasPrefix(b, "/")
	switch {
	case aslash && bslash:
		return a + b[1:]
	case !aslash && !bslash:
		return a + "/" + b
	}
	return a + b
}

func ServeProxy(router, secureRouter *mux.Router, a *auth.Authentication) {
	secureRouter.PathPrefix("/fs/{type}").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleFileProxy(rw, r, a)
		})
}
