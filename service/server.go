// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"log"
	"time"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

const serverKey = "einkeystringder32zeichenlangist."
const userIdKey = "user-id"

func Start() *negroni.Negroni {
	users, err := auth.GetUsers()
	if err != nil {
		log.Fatalf("Could not find any user: %s\n", err)
	}
	if &users == nil {
		log.Fatal("No User found, please run setup.\n")
	}
	n := negroni.New(negroni.NewRecovery(), negroni.NewLogger())

	router := mux.NewRouter()
	secureRoutes := mux.NewRouter().PathPrefix("/").Subrouter()

	a := auth.NewAuthentication(60 * time.Minute)

	//setupStatic(router)

	//setupStarter(secureRoutes, a)
	//setupUser(secureRoutes, a)
	//setupSessions(secureRoutes, a)
	//setupTTY(secureRoutes, a)
	//setupFileservice(secureRoutes, a)
	//setupProxy(secureRoutes, a)

	ServeClient(router)
	ServeLogin(router, secureRoutes, a)
	ServeStarter(router, secureRoutes, a)
	ServeSession(router, secureRoutes, a)
	ServeTTY(router, secureRoutes, a)
	ServeFile(router, secureRoutes, a)
	ServeDocument(router, secureRoutes, a)
	ServeProxy(router, secureRoutes, a)
	ServeAppdata(router, secureRoutes, a)
	ServeFavorites(router, secureRoutes, a)
	ServeUser(router, secureRoutes, a)
	ServeSettings(router, secureRoutes, a)
	ServeWorkspace(router, secureRoutes, a)

	router.PathPrefix("/").Handler(negroni.New(a, negroni.Wrap(secureRoutes)))
	n.UseHandler(router)

	return n
}
