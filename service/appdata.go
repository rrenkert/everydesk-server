// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"log"
	"net/http"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"bitbucket.org/rrenkert/everydesk-server/database"
	"bitbucket.org/rrenkert/everydesk-server/util"

	"github.com/gorilla/mux"
)

func handleGetAppdata(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	db, err := database.Connect()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	defer db.Close()
	userapps := database.Application{}
	db.Where(&user).Related(userapps)

	if err = util.SendJSON(rw, userapps); err != nil {
		log.Printf("util.Send failed: %s\n", err.Error())
	}
}

func ServeAppdata(router, secureRouter *mux.Router, a *auth.Authentication) {
	secureRouter.Path("/appdata").Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleGetAppdata(rw, r, a)
		})
}
