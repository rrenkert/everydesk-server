// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"bitbucket.org/rrenkert/everydesk-server/database"
	"bitbucket.org/rrenkert/everydesk-server/util"
	"github.com/gorilla/mux"
)

func handleGetStarter(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	user, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	var apps []*database.Application
	if user.Admin {
		err = db.Model(&user).Preload("ApplicationType").Related(&apps, "Applications").Error
	} else {
		err = db.Model(&user).Preload("ApplicationType").Where("admin = ?", false).Related(&apps, "Applications").Error
	}
	if err != nil {
		fmt.Println(err)
	}
	// TODO check if apps contains any data
	fmt.Println(&apps)
	if err = util.SendJSON(rw, apps); err != nil {
		log.Printf("Send failed: %s\n", err.Error())
	}
}

func handlePutStarter(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	userID := auth.GetSession(r).Get(userIdKey).(int64)
	loggedIn, err := auth.GetUserById(userID)
	if err != nil {
		log.Printf("DB error: %s\n", err.Error())
		util.HTTPMsg(rw, http.StatusInternalServerError)
		return
	}
	if !loggedIn.Admin {
		if err = util.SendJSON(rw, "{}"); err != nil {
			log.Printf("Send failed: %s\n", err.Error())
		}
		return
	}

	var newApp database.Application
	if err := util.DecodeJSON(r, &newApp); err != nil {
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		// TODO
		util.SendJSON(rw, "error")
		return
	}
	var app database.Application
	err = db.Where("ID = ?", newApp.ID).First(&app).Error
	if err != nil {
		log.Fatal("DB: %s\n", err)
		// TODO
		util.SendJSON(rw, "{error: 'Application not found'}")
		return
	} else {
		app.AppGroup = newApp.AppGroup
		app.Category = newApp.Category
		app.Admin = newApp.Admin
		db.Set("gorm:association_autoupdate", false).Save(&app)
	}
	var apps []database.Application
	db.Find(&apps)
	util.SendJSON(rw, &apps)
}

func handlePostStarter(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	handlePutStarter(rw, r, a)
}

func handleDeleteStarter(rw http.ResponseWriter, r *http.Request, a *auth.Authentication) {
	// TODO
}

func ServeStarter(router, secureRouter *mux.Router, a *auth.Authentication) {
	route := secureRouter.Path("/starter").Subrouter()
	paramRoute := secureRouter.Path("/starter/{id:[a-zA-Z0-9\\.]+}").Subrouter()
	route.Methods("GET").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleGetStarter(rw, r, a)
		})
	route.Methods("PUT").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePutStarter(rw, r, a)
		})
	route.Methods("POST").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handlePostStarter(rw, r, a)
		})
	paramRoute.Methods("DELETE").HandlerFunc(
		func(rw http.ResponseWriter, r *http.Request) {
			handleDeleteStarter(rw, r, a)
		})
}
