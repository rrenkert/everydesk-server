// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package database provides the database model and connection
package database

type User struct {
	ID           int64         `gorm:"PRIMARY_KEY" json:"id"`
	Username     string        `json:"username"`
	Forename     string        `json:"forename"`
	Surname      string        `json:"surname"`
	Email        string        `json:"email"`
	Active       bool          `json:"active"`
	Admin        bool          `json:"admin"`
	External     bool          `json:"external"`
	Secret       string        `json:"-"`
	AesIv        string        `json:"-"`
	Applications []Application `gorm:"many2many:user_applications;" json:"-"`
	Fileservices []Fileservice `json:"-"`
	Favorites    []Favorite    `json:"-"`
}

type FileserviceType struct {
	ID   int64  `gorm:"PRIMARY_KEY" json:"id"`
	Name string `json:"name"`
}

type Fileservice struct {
	ID                int64           `gorm:"PRIMARY_KEY" json:"id"`
	Name              string          `json:"name"`
	Password          string          `json:"password"`
	Path              string          `json:"path"`
	SourcePath        string          `json:"sourcePath"`
	Url               string          `json:"url"`
	FileserviceType   FileserviceType `json:"type"`
	FileserviceTypeID int64           `json:"-"`
	Login             string          `json:"login"`
	UserID            int64           `json:"-"`
}

type ApplicationType struct {
	ID   int    `gorm:"PRIMARY_KEY" json:"id"`
	Name string `json:"name"`
}

type Application struct {
	ID                int64           `gorm:"PRIMARY_KEY" json:"id"`
	Name              string          `json:"name"`
	Category          string          `json:"category"`
	AppGroup          string          `json:"group"`
	Endpoint          string          `json:"endpoint"`
	Icon              string          `json:"icon"`
	ApplicationType   ApplicationType `json:"type"`
	ApplicationTypeID int             `json:"-"`
	Admin             bool            `json:"admin"`
}

type FavoriteType struct {
	ID   int64  `gorm:"PRIMARY_KEY" json:"id"`
	Name string `json:"name"`
}

// Favorite contains the users most used items
type Favorite struct {
	ID             int          `gorm:"PRIMARY_KEY" json:"id"`
	Name           string       `json:"name"`
	FavoriteType   FavoriteType `gorm:"association_autoupdate:false" json:"type"`
	FavoriteTypeID int64        `json:"-"`
	Endpoint       string       `json:"endpoint"`
	URI            string       `json:"uri"`
	Icon           string       `json:"icon"`
	UserID         int64        `json:"-"`
	Itemgroup      string       `json:"group"`
}

type Workspace struct {
	ID     int    `gorm:"PRIMARY_KEY" json:"id"`
	Name   string `json:"name"`
	State  string `sql:"type:JSONB NOT NULL DEFAULT '[]'::JSONB" json:"state"`
	UserID int64  `json:"-"`
}

type Settings struct {
	ID         int    `gorm:"PRIMARY_KEY" json:"id"`
	Background string `json:"background"`
	UserID     int64  `json:"-"`
}
