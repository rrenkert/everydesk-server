// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package setup

import (
	"log"

	"bitbucket.org/rrenkert/everydesk-server/database"
)

func CreateTables() {
	db, err := database.Connect()
	defer db.Close()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	if !db.HasTable(&database.User{}) {
		db.CreateTable(&database.User{})
	}
	if !db.HasTable(&database.FileserviceType{}) {
		db.CreateTable(&database.FileserviceType{})
		db.Create(&database.FileserviceType{
			Name: "webdav",
		})
		db.Create(&database.FileserviceType{
			Name: "dropbox",
		})
	}
	if !db.HasTable(&database.Fileservice{}) {
		db.CreateTable(&database.Fileservice{})
		db.Model(&database.Fileservice{}).AddForeignKey("fileservice_type_id", "fileservice_types(id)", "CASCADE", "CASCADE")
	}
	if !db.HasTable(&database.ApplicationType{}) {
		db.CreateTable(&database.ApplicationType{})
		db.Create(&database.ApplicationType{
			ID:   0,
			Name: "app",
		})
	}
	if !db.HasTable(&database.Application{}) {
		db.CreateTable(&database.Application{})
		db.Model(&database.Application{}).AddForeignKey("application_type_id", "application_types(id)", "CASCADE", "CASCADE")
		db.Create(&database.Application{
			Name:              "Editor",
			AppGroup:          "",
			Category:          "office",
			Endpoint:          "everydesk.plugin.editor.Editor",
			Icon:              "livejournal.png",
			ApplicationTypeID: 1,
			Admin:             false,
		})
		db.Create(&database.Application{
			Name:              "Office",
			AppGroup:          "",
			Category:          "office",
			Endpoint:          "everydesk.plugin.office.Office",
			Icon:              "category.png",
			ApplicationTypeID: 1,
			Admin:             false,
		})
		db.Create(&database.Application{
			Name:              "Settings",
			AppGroup:          "",
			Category:          "system",
			Endpoint:          "everydesk.plugin.settings.Settings",
			Icon:              "setting_tools.png",
			ApplicationTypeID: 1,
			Admin:             false,
		})
		db.Create(&database.Application{
			Name:              "Filebrowser",
			AppGroup:          "",
			Category:          "",
			Endpoint:          "everydesk.plugin.filebrowser.Filebrowser",
			Icon:              "folder_vertical_document.png",
			ApplicationTypeID: 1,
			Admin:             false,
		})
		db.Create(&database.Application{
			Name:              "ImageViewer",
			AppGroup:          "",
			Category:          "",
			Endpoint:          "everydesk.plugin.imgviewer.ImageViewer",
			Icon:              "images.png",
			ApplicationTypeID: 1,
			Admin:             false,
		})
		db.Create(&database.Application{
			Name:              "AdminPanel",
			AppGroup:          "",
			Category:          "system",
			Endpoint:          "everydesk.plugin.admin.AdminPanel",
			Icon:              "administrator.png",
			ApplicationTypeID: 1,
			Admin:             true,
		})
	}
	if !db.HasTable(&database.FavoriteType{}) {
		db.CreateTable(&database.FavoriteType{})
		db.Create(&database.FavoriteType{
			Name: "app",
		})
		db.Create(&database.FavoriteType{
			Name: "file",
		})
		db.Create(&database.FavoriteType{
			Name: "url",
		})
	}
	if !db.HasTable(&database.Favorite{}) {
		db.CreateTable(&database.Favorite{})
		db.Model(&database.Favorite{}).AddForeignKey("favorite_type_id", "favorite_types(id)", "CASCADE", "CASCADE")
	}
	if !db.HasTable(&database.Workspace{}) {
		db.CreateTable(&database.Workspace{})
	}
	if !db.HasTable(&database.Settings{}) {
		db.CreateTable(&database.Settings{})
	}
}
