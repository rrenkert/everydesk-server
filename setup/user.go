// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package setup

import (
	"log"

	"github.com/jinzhu/gorm"

	"bitbucket.org/rrenkert/everydesk-server/auth"
	"bitbucket.org/rrenkert/everydesk-server/backend"
	"bitbucket.org/rrenkert/everydesk-server/database"
)

func CreateUser() {
	db, err := database.Connect()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	defer db.Close()
	var admin []*database.User
	db.Where("Admin = ?", true).Find(&admin)
	log.Println("Looking for admin user.")
	if len(admin) == 0 {
		log.Println("Not found. Creating admin user.")
		newAdmin := database.User{
			Username: "admin",
			Forename: "admin",
			Surname:  "admin",
			Email:    "admin@localhost",
			Active:   true,
			Admin:    true,
			External: false,
			Secret:   auth.HashPassword("secret"),
			AesIv:    auth.GenerateIV(),
		}
		db.Create(&newAdmin)
		if err != nil {
			log.Fatal(err)
		}
		log.Println("Done.")
		log.Println("Create App associations")
		var apps []*database.Application
		db.Find(&apps)
		for _, app := range apps {
			log.Println("App: ", app.Name)
			db.Model(&newAdmin).Association("Applications").Append(database.Application{ID: app.ID})
		}
		log.Println("Done.")
	} else {
		log.Printf("Found admin users (%d). Skipping.\n", len(admin))
	}
	SyncUsersLdap()
}

func SyncUsersLdap() {
	db, err := database.Connect()
	if err != nil {
		log.Fatal("Create or open DB: %s\n", err)
		return
	}
	defer db.Close()
	ldap := backend.Ldap{}
	err = ldap.Connect()
	if err != nil {
		log.Fatal("Connect to LDAP: %s\n", err)
		return
	}
	defer ldap.Disconnect()

	users, err := ldap.GetUsers()
	if err != nil {
		log.Fatal(err)
	}
	var apps []*database.Application
	db.Find(&apps)
	for i := range users {
		if users[i]["uid"] == "admin" {
			continue
		}
		u, err := auth.GetUser(users[i]["uid"])
		if err != nil && !gorm.IsRecordNotFoundError(err) {
			log.Fatal("Database error")
			return
		}
		if err != nil && gorm.IsRecordNotFoundError(err) {
			newUser := database.User{
				Username: users[i]["uid"],
				Forename: users[i]["givenName"],
				Surname:  users[i]["sn"],
				Email:    users[i]["mail"],
				External: true,
				Active:   true,
				Admin:    false,
				AesIv:    auth.GenerateIV(),
			}
			db.Create(&newUser)
			log.Printf("Create user %s\n", users[i]["uid"])
			for _, app := range apps {
				log.Println("App: ", app.Name)
				db.Model(&newUser).Association("Applications").Append(database.Application{ID: app.ID})
			}
			continue
		}
		if u.Username == users[i]["uid"] {
			log.Printf("User %s exists.", u.Username)
			continue
		}
	}
}
