// Copyright © 2016 Raimund Renkert <raimund@renkert.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package backend

import (
	"errors"
	"fmt"
	"log"

	ldapclient "github.com/jtblin/go-ldap-client"
	"github.com/spf13/viper"
	"gopkg.in/ldap.v2"
)

type Ldap struct {
	client *ldapclient.LDAPClient
}

func (ldapClient *Ldap) Connect() error {
	ldapClient.client = &ldapclient.LDAPClient{
		Base:         viper.GetString("ldap.basedn"),
		Host:         viper.GetString("ldap.host"),
		Port:         viper.GetInt("ldap.port"),
		UseSSL:       viper.GetBool("ldap.usessl"),
		SkipTLS:      viper.GetBool("ldap.skiptls"),
		BindDN:       viper.GetString("ldap.binddn"),
		BindPassword: viper.GetString("ldap.bindpassword"),
		UserFilter:   viper.GetString("ldap.userfilter"),
		GroupFilter:  "(memberUid=%s)",
		Attributes:   []string{"uid", "givenName", "sn", "mail"},
	}
	err := ldapClient.client.Connect()
	if err != nil {
		return err
	}
	return nil
}

func (ldapClient *Ldap) Disconnect() {
	ldapClient.client.Close()
}

func (ldapClient *Ldap) Authenticate(name string, password string) (bool, error) {
	if ldapClient.client == nil {
		return false, errors.New("Not connected to LDAP")
	}
	ok, _, err := ldapClient.client.Authenticate(name, password)
	if err != nil {
		return false, err
	}
	if !ok {
		return false, errors.New("Incorrect Password")
	}
	return true, nil
}

func (ldapClient *Ldap) GetUser(username string) (map[string]string, error) {
	if ldapClient.client == nil {
		return nil, errors.New("Not connected to LDAP")
	}
	// First bind with a read only user
	if ldapClient.client.BindDN != "" && ldapClient.client.BindPassword != "" {
		err := ldapClient.client.Conn.Bind(ldapClient.client.BindDN, ldapClient.client.BindPassword)
		if err != nil {
			return nil, err
		}
	}

	attributes := append(ldapClient.client.Attributes, "dn")
	// Search for the given username
	searchRequest := ldap.NewSearchRequest(
		ldapClient.client.Base,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		fmt.Sprintf(viper.GetString("ldap.userfilter"), username),
		attributes,
		nil,
	)

	sr, err := ldapClient.client.Conn.Search(searchRequest)
	if err != nil {
		return nil, err
	}

	if len(sr.Entries) < 1 {
		return nil, errors.New("No user found")
	}
	if len(sr.Entries) > 1 {
		return nil, errors.New("Too many entries returned")
	}

	user := map[string]string{}
	for _, attr := range ldapClient.client.Attributes {
		user[attr] = sr.Entries[0].GetAttributeValue(attr)
	}
	return user, nil
}

func (ldapClient *Ldap) GetUsers() ([]map[string]string, error) {
	if ldapClient.client == nil {
		return nil, errors.New("Not connected to LDAP")
	}
	// First bind with a read only user
	if ldapClient.client.BindDN != "" && ldapClient.client.BindPassword != "" {
		err := ldapClient.client.Conn.Bind(ldapClient.client.BindDN, ldapClient.client.BindPassword)
		if err != nil {
			return nil, err
		}
	}

	attributes := append(ldapClient.client.Attributes, "dn")
	// Search for the given username
	searchRequest := ldap.NewSearchRequest(
		ldapClient.client.Base,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		"(|(objectclass=inetOrgPerson))",
		attributes,
		nil,
	)

	sr, err := ldapClient.client.Conn.Search(searchRequest)
	if err != nil {
		return nil, err
	}

	if len(sr.Entries) < 1 {
		return nil, errors.New("No users found")
	}

	users := make([]map[string]string, 0)
	for i := range sr.Entries {
		user := map[string]string{}
		groups, err := ldapClient.client.GetGroupsOfUser(sr.Entries[i].GetAttributeValue("uid"))
		if err != nil {
			log.Fatalf("Error getting groups: %+v", err)
			continue
		}
		sync := false
		for _, g := range groups {
			if viper.GetString("ldap.groupfilter") == g {
				sync = true
			}
		}
		if !sync {
			continue
		}
		for _, attr := range ldapClient.client.Attributes {
			user[attr] = sr.Entries[i].GetAttributeValue(attr)
		}
		users = append(users, user)
	}
	return users, nil
}
