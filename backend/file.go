package backend

import "time"

type File struct {
	Name     string    `json:"name"`
	Path     string    `json:"path"`
	Size     uint64    `json:"size"`
	Type     string    `json:"type"`
	Created  time.Time `json:"created"`
	Modified time.Time `json:"modified"`
	MimeType string    `json:"mimeType"`
	Version  string    `json:"version"`
}
