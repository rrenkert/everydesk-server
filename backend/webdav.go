package backend

import (
	"crypto/tls"
	"encoding/base64"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
	"strings"
	"time"
)

type Webdav struct {
	url     string
	headers http.Header
	client  *http.Client
}

type props struct {
	Status   string   `xml:"DAV: status"`
	Name     string   `xml:"DAV: prop>displayname,omitempty"`
	Type     xml.Name `xml:"DAV: prop>resourcetype>collection,omitempty"`
	Size     string   `xml:"DAV: prop>getcontentlength,omitempty"`
	Modified string   `xml:"DAV: prop>getlastmodified,omitempty"`
	MimeType string   `xml:"DAV: prop>getcontenttype,omitempty"`
	Version  string   `xml:"DAV: prop>getetag,omitempty"`
}

type response struct {
	Href  string  `xml:"DAV: href"`
	Props []props `xml:"DAV: propstat"`
}

// NewWebdav creates a new webdav connection with user and password as BasicAuth
func NewWebdav(url string, user string, password string) (*Webdav, error) {
	webdav := &Webdav{url, make(http.Header), &http.Client{}}
	webdav.client.Transport = &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	if len(user) <= 0 || len(password) <= 0 {
		return nil, errors.New("No User or Password")
	}

	auth := user + ":" + password
	authBase64 := "Basic " + base64.StdEncoding.EncodeToString([]byte(auth))
	webdav.headers.Add("Authorization", authBase64)
	return webdav, nil
}

// Ls lists the content of 'path' and replaces 'prefix' with 'orig' in path
func (w *Webdav) Ls(path string, prefix string, orig string) ([]File, error) {
	path = fixSlash(path)
	files := make([]File, 0)
	body := "<?xml version='1.0' encoding='utf-8' ?><propfind xmlns='DAV:'><allprop/></propfind>"
	resp, err := w.request("PROPFIND", path, strings.NewReader(body), func(request *http.Request) {
		request.Header.Add("Depth", "1")
		request.Header.Add("Content-Type", "text/xml;charset=UTF-8")
		request.Header.Add("Accept", "application/xml,text/xml")
		request.Header.Add("Accept-Charset", "utf-8")
	})
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 207 {
		return nil, fmt.Errorf("%s - %s", resp.Status, path)
	}
	err = parseXML(resp.Body, &response{}, func(resp interface{}) error {
		r := resp.(*response)
		if p := getProps(r, "200"); p != nil {
			f := new(File)
			name := ""
			if ps, err := url.QueryUnescape(r.Href); err == nil {
				name = ps
			} else {
				name = p.Name
			}
			name = strings.TrimPrefix(name, prefix)
			path = strings.TrimSuffix(path, "/")
			name = strings.TrimPrefix(name, path)
			f.Path = orig + path + name
			name = strings.TrimSuffix(name, "/")
			f.Name = strings.TrimPrefix(name, "/")
			f.Modified = parseDate(&p.Modified)

			if p.Type.Local == "collection" {
				f.Size = 0
				f.Type = "folder"
				f.MimeType = "folder"
			} else {
				f.Size = parseInt64(&p.Size)
				f.Type = "file"
				f.MimeType = p.MimeType
				f.Version = p.Version
			}
			files = append(files, *f)
		}
		r.Props = nil
		return nil
	})
	if err != nil {
		return nil, err
	}
	return files, nil
}

// Get proxies the request to the webdav backend and pipes the requested file to the client
func (w *Webdav) Get(path, prefix string, rw http.ResponseWriter, r *http.Request) error {
	u, err := url.Parse(w.url)
	if err != nil {
		return err
	}
	proxy := httputil.NewSingleHostReverseProxy(u)
	proxy.Director = func(r *http.Request) {
		r.URL.Scheme = u.Scheme
		r.URL.Host = u.Host
		r.URL.Path = strings.TrimPrefix(r.URL.Path, prefix)
		r.URL.Path = singleJoiningSlash(u.Path, r.URL.Path)
		r.Header.Set("Authorization", w.headers.Get("Authorization"))
		r.Header.Set("X-Forwarded-Host", r.Header.Get("Host"))
		r.Header.Set("Host", r.URL.Host)
		r.Header.Set("Referer", r.URL.String())
		r.Host = u.Host
	}
	proxy.Transport = &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	proxy.ServeHTTP(rw, r)
	return nil
}

func (w *Webdav) Save(path string, rw http.ResponseWriter, r *http.Request) error {
	resp, err := w.request("PUT", path, r.Body, nil)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

func (w *Webdav) SaveReader(path string, content io.Reader) error {
	_, err := w.request("PUT", path, content, nil)
	if err != nil {
		return err
	}
	return nil
}

func (w *Webdav) Rm(path string) (*http.Response, error) {
	resp, err := w.request("DELETE", path, nil, nil)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return resp, nil
}

func (w *Webdav) Mv(src, dest string) (*http.Response, error) {
	path, err := url.Parse(w.url)
	if err != nil {
		return nil, err
	}
	dest = path.Path + dest
	resp, err := w.request("MOVE", src, nil, func(request *http.Request) {
		request.Header.Add("Destination", dest)
		request.Header.Add("Overwrite", "F")
	})
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return resp, nil
}

func (w *Webdav) Mkdir(path string) (*http.Response, error) {
	resp, err := w.request("MKCOL", path, nil, nil)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return resp, nil
}

func (w *Webdav) request(method string, path string, body io.Reader, intercept func(*http.Request)) (res *http.Response, err error) {
	request, err := http.NewRequest(method, join(w.url, path), body)
	if err != nil {
		return nil, err
	}
	for i, values := range w.headers {
		for _, value := range values {
			request.Header.Add(i, value)
		}
	}
	if intercept != nil {
		intercept(request)
	}
	return w.client.Do(request)
}

func parseXML(data io.Reader, resp interface{}, parse func(resp interface{}) error) error {
	decoder := xml.NewDecoder(data)
	first := true
	for t, _ := decoder.Token(); t != nil; t, _ = decoder.Token() {
		switch se := t.(type) {
		case xml.StartElement:
			if se.Name.Local == "response" {
				if first {
					first = false
					continue
				}
				if e := decoder.DecodeElement(resp, &se); e == nil {
					if err := parse(resp); err != nil {
						return err
					}
				}
			}
		}
	}
	return nil
}

func getProps(r *response, status string) *props {
	for _, prop := range r.Props {
		if strings.Index(prop.Status, status) != -1 {
			return &prop
		}
	}
	return nil
}

func parseInt64(s *string) uint64 {
	if n, e := strconv.ParseUint(*s, 10, 64); e == nil {
		return n
	}
	return 0
}

func parseDate(s *string) time.Time {
	if t, e := time.Parse(time.RFC1123, *s); e == nil {
		return t
	}
	return time.Unix(0, 0)
}

func join(path0 string, path1 string) string {
	return strings.TrimSuffix(path0, "/") + "/" + strings.TrimPrefix(path1, "/")
}

func singleJoiningSlash(a, b string) string {
	aslash := strings.HasSuffix(a, "/")
	bslash := strings.HasPrefix(b, "/")
	switch {
	case aslash && bslash:
		return a + b[1:]
	case !aslash && !bslash:
		return a + "/" + b
	}
	return a + b
}

func fixSlash(path string) string {
	if path[0] != '/' {
		path = "/" + path
	}
	if !strings.HasSuffix(path, "/") {
		path += "/"
	}
	return path
}
