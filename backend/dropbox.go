package backend

import (
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/dropbox/dropbox-sdk-go-unofficial/dropbox"
	"github.com/dropbox/dropbox-sdk-go-unofficial/dropbox/files"
	"github.com/spf13/viper"
	"golang.org/x/oauth2"
)

type DBox struct {
	config dropbox.Config
}

func GetDropboxOAuthUrl() string {
	oAuthConfig := oauth2.Config{
		ClientID:     viper.GetString("dropbox.appId"),
		ClientSecret: viper.GetString("dropbox.appSecret"),
		Endpoint:     dropbox.OAuthEndpoint(""),
	}
	return oAuthConfig.AuthCodeURL("state")
}

func NewDBox(token string) *DBox {
	config := dropbox.Config{Token: token, LogLevel: dropbox.LogOff}
	dbox := &DBox{config}
	return dbox
}

func (d *DBox) Ls(path string, prefix string) ([]File, error) {
	dbxFiles := files.New(d.config)
	list, err := dbxFiles.ListFolder(files.NewListFolderArg(path))
	if err != nil {
		return nil, err
	}
	fs := make([]File, 0)
	for i := 0; i < len(list.Entries); i++ {
		f := new(File)
		switch e := list.Entries[i].(type) {
		case *files.FileMetadata:
			f.Name = e.Name
			f.Created = e.ServerModified
			f.Modified = e.ClientModified
			f.Path = prefix + e.PathLower
			f.Size = e.Size
			f.Type = "file"
			f.MimeType = findMimeType(e.Name)
			f.Version = e.Rev
		case *files.FolderMetadata:
			f.Name = e.Name
			f.Path = prefix + e.PathLower + "/"
			f.Size = 0
			f.Type = "folder"
			f.MimeType = "folder"
		}
		fs = append(fs, *f)
	}
	return fs, nil
}

func (dbox *DBox) Get(path string, rw http.ResponseWriter) error {
	arg := files.NewDownloadArg(path)
	db := files.New(dbox.config)
	read, write := io.Pipe()
	go func() {
		defer write.Close()
		_, content, err := db.Download(arg)
		defer content.Close()
		if err != nil {
			fmt.Println("error")
			fmt.Println(err)
		}
		io.Copy(write, content)
	}()
	io.Copy(rw, read)
	return nil
}

func (dbox *DBox) Save(path string, r *http.Request) error {
	commitInfo := files.NewCommitInfo(path)
	commitInfo.Mode.Tag = "overwrite"
	commitInfo.ClientModified = time.Now().UTC().Round(time.Second)
	db := files.New(dbox.config)
	_, err := db.Upload(commitInfo, r.Body)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func (dbox *DBox) SaveReader(path string, content io.Reader) error {
	commitInfo := files.NewCommitInfo(path)
	commitInfo.Mode.Tag = "overwrite"
	commitInfo.ClientModified = time.Now().UTC().Round(time.Second)
	db := files.New(dbox.config)
	_, err := db.Upload(commitInfo, content)
	if err != nil {
		return err
	}
	return nil
}

func (dbox *DBox) Rm(path string) error {
	path = strings.TrimSuffix(path, "/")
	folderArg := files.NewDeleteArg(path)
	db := files.New(dbox.config)
	_, err := db.DeleteV2(folderArg)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func (dbox *DBox) Mv(path, dest string) error {
	folderArg := files.NewRelocationArg(path, dest)
	db := files.New(dbox.config)
	_, err := db.MoveV2(folderArg)
	if err != nil {
		return err
	}
	return nil
}

func (dbox *DBox) Mkdir(path string) error {
	arg := files.NewCreateFolderArg(path)

	dbx := files.New(dbox.config)
	if _, err := dbx.CreateFolderV2(arg); err != nil {
		return err
	}
	return nil
}

func findMimeType(fileName string) string {
	if strings.HasSuffix(fileName, ".jpg") {
		return "image/jpeg"
	}
	if strings.HasSuffix(fileName, ".mp3") {
		return "audio/mp3"
	}
	if strings.HasSuffix(fileName, ".dat") {
		return "application/octet-stream"
	}
	if strings.HasSuffix(fileName, ".pdf") {
		return "application/pdf"
	}
	if strings.HasSuffix(fileName, ".odt") {
		return "application/vnd.oasis.opendocument.text"
	}
	if strings.HasSuffix(fileName, ".docx") {
		return "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
	}
	return ""
}
