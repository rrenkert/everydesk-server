FROM alpine:latest

MAINTAINER raimund@renkert.org

RUN adduser -DH everydesk

USER everydesk

ADD ./everydesk.toml /etc/everydesk/everydesk.toml
ADD ./everydesk-server /opt/everydesk-server
ADD ./everydesk /var/www/everydesk

RUN /opt/everydesk-server setup

EXPOSE 2808

CMD ["/opt/everydesk-server"]
