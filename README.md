# everydesk-server #

Server commponent for the 'everywhere browser desktop'.

Serves the client, a proxy and connects to a CouchDB to store configurations.

All requests are authorized using a HTTP-Header token. This authorization maps the user to external Systems (e.g. ownclouds webdav service)

### Info ###

* Build it using `go get && go build`
* Version: 0.0.1 (no RC!)